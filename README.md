# simplestRCI

## Simplest example of GitLab CI for R.

- This is purely an accompanying project for the [How to easily automate R analysis, modeling and development work using CI/CD, with working examples](https://jozefhajnala.gitlab.io/r/r106-r-package-gitlab-ci/) blogpost
- The [.gitlab-ci.yml](https://gitlab.com/jozefhajnala/simplestrci/blob/master/.gitlab-ci.yml) file
- An overview of the [pipeline runs](https://gitlab.com/jozefhajnala/simplestrci/pipelines)
- An example [output of a run](https://gitlab.com/jozefhajnala/simplestrci/-/jobs/194646370)

## For more interesting CI/CD pipelines, look at:

### R package testing

- The [.gitlab-ci.yml](https://gitlab.com/jozefhajnala/jhaddins/blob/develop/.gitlab-ci.yml) file for the `jhaddins` package on branch develop
- An overview of the [pipeline runs](https://gitlab.com/jozefhajnala/jhaddins/pipelines)
- An example [output of a successful run](https://gitlab.com/jozefhajnala/jhaddins/pipelines/55954699)
- An example output of a run that [discovered a NOTE](https://gitlab.com/jozefhajnala/jhaddins/-/jobs/139893165) in the check process

### Building a Docker image for R

The Docker image `jozefhajnala/rdev:3.4.4` used for the above

- is based on the following [Dockerfile](https://gitlab.com/jozefhajnala/dockerfiles/blob/master/r3.4.4/Dockerfile)
- is also built with GitLab CI/CD, look at the [.gitlab-ci.yml file](https://gitlab.com/jozefhajnala/dockerfiles/blob/master/.gitlab-ci.yml) 
- the build [pipeline in action](https://gitlab.com/jozefhajnala/dockerfiles/-/jobs/194667135)